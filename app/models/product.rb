class Product < ApplicationRecord
	has_many :images

	serialize :tags, Array
	serialize :categories, Array
end
