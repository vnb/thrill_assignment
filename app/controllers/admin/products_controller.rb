class Admin::ProductsController < ApplicationController
  before_action :set_product, only: [:update, :edit]

  def index
    @products = Product.page(params[:page]).per(25).order('id DESC')
    @user = current_user
  end

  def edit
    @product
  end


  ###ASSUMPTIONS
  #  Tags and Categories are separated by space when editing
  ###
  def update
    categories = product_params["categories"].split(" ").map {|item| item.squish()}
    tags = product_params["tags"].split(" ").map {|item| item.squish()}
    processed_params = product_params.merge({"categories" => categories, "tags" => tags})
    respond_to do |format|
      if @product.update(processed_params)
        format.html { redirect_to admin_products_path, notice: 'Product was successfully updated.' }
        format.json { render :show, status: :ok, location: @product }
      else
        format.html { render :edit }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    def set_product
      # binding.pry
      @product = Product.find(params[:id])
    end

    def product_params
      params.require(:product).permit(:name, :sku_id, :categories, :tags, :price, :expire_date)
    end
end
