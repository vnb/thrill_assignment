class Api::V1::ProductsController < ApiController
  before_action :set_product, only: [:update, :create]


  ###ASSUMPTIONS
  #  Triggers When a POST request with body is triggered
  ###
  def create
    if product_params.present?
      if @product.nil?
        @new_product = Product.new(product_params.except("images"))
          if @new_product.save
            product_params["images"].each do |img_obj|
              Image.create({"img_path" => img_obj["img_path"], "product_id" => @new_product.id})
            end
            render status: 200, json: {"message" => "successfully created"} and return
          else
            render status: 500, json: {"message" => "#{@product.errors}" } and return
          end
      else
        render :update
      end

    else
      render status: 422, json: {"message" => "Improper Format"} and return
    end
  end

  ###ASSUMPTIONS
  #  When a get request with body is triggered
  #  Check for method in json and process accordingly
  ###

  def index
    method_passed = params.fetch("method", "").upcase
    case method_passed
      when ""
        render status: 422, json: {"message" => "No Method Defined"} and return
      when "POST"
        render :create
      when "PUT"
        render :update
      else
        render status: 422, json: {"message" => "Method out of scope"} and return
      end
  end

  ###ASSUMPTIONS
  #  Triggers When a PUT request with body is triggered
  ###
  def update
    if @product
      @product.attributes = product_params.except("images")
      if @product.changed?
        if @product.save
          render status: 200, json: {"message" => "successfully updated"} and return
        else
          render status: 500, json: {"message" => "#{@product.errors}" } and return
        end
      else
        render status: 304, json: {"message" => "No new data to save"} and return
      end
    else
      render :create
    end
  end

  private

    ###ASSUMPTION
    #  Product is assumed to be unique if the combination of
    #  Name, Price and SKU Id is unique
    #####
    def set_product
      # @product = Product.find(:all, :conditions => ['name LIKE ? AND price LIKE ? AND sku_id LIKE ?', "%#{product_params.fetch('name', 'NULL')}%", "%#{product_params.fetch('price', 'NULL')}%", "%#{product_params.fetch('sku_id', 'NULL')}%"])
      @product = Product.where('name LIKE :name AND price LIKE :price AND sku_id LIKE :sku_id', name: "%#{product_params.fetch('name', 'NULL')}%", price: "%#{product_params.fetch('price', 'NULL')}%", sku_id: "%#{product_params.fetch('sku_id', 'NULL')}%").first

    end

    def product_params
      params.require(:parameters).permit(:name, :sku_id, {categories: []}, {tags: []}, {images: [:img_path]}, :price, :expire_date)
    end
end
