Rails.application.routes.draw do

  devise_for :users
	root to: "products#index"
  resources :users
  # resources :images
  # resources :products
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resources :products, except: [:edit, :index, :create, :show, :new, :update, :destroy] do
      	collection do
	      	post '/' => 'products#create'
	      	put '/' => 'products#update'
	      	get '/' => 'products#index'
	      end
      end
    end
  end

  namespace :admin do
    resources :products, except: [:create, :show, :new, :destroy]
  end

end
