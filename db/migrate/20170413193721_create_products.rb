class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products do |t|
      t.string :name
      t.integer :sku_id
      t.string :categories
      t.string :tags
      t.integer :price
      t.datetime :expire_date

      t.timestamps
    end
  end
end
