# README

* Ruby Version: 2.3.1

* Rails Version: 5.0.0.1

* Database: MYSQL

Dependencies: 

* Postman/application of your choice to make api calls

Gems added:

* mysql2

* pry

* kaminari

* devise

After Cloning the project, run the following commands

* bundle install

* Change the database.yml configuration to reflect you credentials. Using MYSQL. 

Then in terminal run the following commands,

* rake db:create

* rake db:migrate

* rake db:seed #To create users or you can sign up using application

Seed data for post and get request is in seed_data.json file in root directory

Run "rails s" to run the server. Application opens in http://localhost:3000
